var expect = require("chai").expect;
var powerTest = require("../app/pow");

describe("potenza", function(){
it ("test potenza",function()
    {var potenza1 =powerTest.pow(2,3)
    var potenza2 =powerTest.pow(4,3)
expect(potenza1).to.equal(8);
expect(potenza2).to.equal(64);
})
it ("test potenza2 ",function()
    {var potenza1 =powerTest.pow(5,3)
    var potenza2 =powerTest.pow(10,3)
expect(potenza1).to.equal(124);
expect(potenza2).to.equal(1000);
})
})